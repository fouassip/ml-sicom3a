close all
clear all
%% Load synthetic data sets (see question 1.1)

nom_fich_train= 'synth_train.mat' ;
nom_fich_test= 'synth_test.mat' ; 

load(nom_fich_train) ;% import training dataset into variables Xtrain, Ytrain
load(nom_fich_test) ; % import test dataset into variables Xtest, Ytest

% Get the sample size n and the data point dimension p (here p=2)
[n,p]= size( Xtrain )

% binary classification problem
K=2 ; 

%% Training set 

% Get the index of the samples for ecah class separetely 
ind1= (Ytrain==1) ;
ind2= (Ytrain==2) ;

% display bivariate data points (see lab question 1.1)
figure,
hold on
plot(Xtrain(ind1,1), Xtrain(ind1,2),'+r') ;
plot(Xtrain(ind2,1), Xtrain(ind2,2),'oc' );


%% Train LDA classifier by hand (see Lab question 1.2)

% Compute (unbiased) MLE

ind1= (Ytrain==1) ;
ind2= (Ytrain==2) ;
n1= sum(ind1) ;
n2= sum(ind2) ;

% weights \pi_k for each class
pi1hat= n1/n ;
pi2hat= n2/n ;

% mean vector \mu_k for each class
mu1hat= mean( Xtrain(ind1,:) ) ;
mu1hat= mu1hat(:) ; % get a column vector
mu2hat= mean( Xtrain(ind2,:) ) ;
mu2hat= mu2hat(:) ; % get a column vector

% common pooled covariance matrix

% sample covariances C_k for each class
% Here covkhat= 1 / (nk-1) * sum_{for i in indk} ( X_i - mukhat) * ( X_i - mukhat)' 
cov1hat= cov( Xtrain(ind1,:) ) ;
cov2hat= cov( Xtrain(ind2,:) ) ;

% TODO (Lab question 1.2)
%  - compute the pooled sample covarance SigmaLDAhat
%  - compute the coeffs for the decision boundary equation: 
%    constant CLDA and linear vector LLDA 
%  - plot the decision boundary
%  - compute misclassification rate

SigmaLDAhat= eye(p) ; %FIXME 
CLDA= 0 ; %FIXME
LLDA= ones(2,1) ; %FIXME

% Plot the decision boudary
boundaryLDA= @(x,y) ( CLDA + LLDA(:)' * [x(:)' ; y(:)']    ) ; % boundary function
h= ezplot( boundaryLDA ,[-3 3 -1 4] ) ; % Recent versions of matlab warn to replace ezplot() by fimplicit(), but that's okay
set(h, 'Color', 'k','LineWidth',2); % draw a black solid line

%% Compare with matlab built-in functions for LDA/QDA classification (Lab questions 1.3 and 1.4)
%  fitcdiscr() to fit discriminant analysis classifier (training step). See the 'discrimType' parameters in the doc 
%  predict() for prediction step
%  Note: in older versions of matlab, fitcdiscr() is named
%  ClassificationDiscriminant.fit()

% Exemple of call for LDA
mdl = fitcdiscr(Xtrain,Ytrain) ;
% Coeffs for the decision boundary equation between class 1 and 2 are then 
% stored in the following structure. See fields Const and Linear
mdl.Coeffs(1,2)
% Classify test data
Yhat= predict(mdl,Xtest) ;

% TODO: Complete the lab questions
