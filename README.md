# Python notebooks for Sicom ML course

You will find in this repository the material associated with the course:

- Slides (pdf files) for the lessons,
- **Examples with [Jupyter notebooks](./notebooks/)** to illustrate concepts and methods in Python (.ipynb files)

## Requirements to run notebooks:

**Two** solutions:

1. Python (> 3.3), Jupyter notebook and scikit-learn package. It is recommended to install them via Anaconda Distribution which will install these dependencies directly.

**Or**

2. Use the mybinder service to run them remotely and interactively (wait a few seconds for the first connection so that the environment loads):

- [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fchatelaf%2Fml-sicom3a/54301940e4486a8ece22a910c3efa1b2734ed82d?filepath=notebooks)
  link to run the examples, *except Deep learning ones* too computationally demanding for the JupyterHub server (use the first solution to run these notebooks with your own ressources...)
